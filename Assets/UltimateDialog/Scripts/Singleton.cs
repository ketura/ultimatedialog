﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
  protected static T instance;

  public static T Instance
  {
    get
    {
      if(instance == null)
      {
        instance = (T) FindObjectOfType(typeof(T));

        if(instance == null)
        {
          GameObject go = GameObject.Find("_Generated_" + typeof(T) + "_Singleton");
          if (go != null)
          {
            instance = go.GetComponent<T>();
            Debug.Log("had to search.");
          }
        }
 
        if (instance == null)
        {
          Debug.Log("An instance of " + typeof(T) + " is needed in the scene, but there is none. Creating new instance.");
          GameObject inst = new GameObject("_Generated_" + typeof(T) + "_Singleton");
          inst.AddComponent<T>();
          instance = inst.GetComponent<T>();
        }
      }
 
      return instance;
    }
  }
}
