﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ColorTheme : MonoBehaviour 
{
  public Color Control;

  public Color TreeHighlight;
  public Color TreeSelect;

  public ColorBlock TreeHighlightBlock { get; private set; }
  public ColorBlock TreeSelectBlock { get; private set; }

  void Awake()
  {
    ColorBlock block = ColorBlock.defaultColorBlock;
    block.highlightedColor = TreeHighlight;
    block.pressedColor = TreeSelect;
    TreeHighlightBlock = block;

    block = ColorBlock.defaultColorBlock;
    block.normalColor = TreeSelect;
    block.highlightedColor = TreeSelect;
    block.pressedColor = TreeHighlight;
    TreeSelectBlock = block;
  }

}
