﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogSkinManager : Singleton<DialogSkinManager> 
{
  public DialogSkin[] StartingSkins;
  public DialogSkin CurrentSkin;


  protected Dictionary<string, DialogSkin> Skins;

  void Awake () 
  {
    Skins = new Dictionary<string, DialogSkin>();

    foreach(DialogSkin skin in StartingSkins)
      Skins.Add(skin.SkinName, skin);

    if (CurrentSkin == null)
      foreach(DialogSkin skin in Skins.Values)
      {
        CurrentSkin = skin;
        break;
      }
  }
  
  void Update () 
  {
  
  }

  public DialogSkin GetSkin(string name)
  {
    if (!Skins.ContainsKey(name))
      return null;

    return Skins[name];
  }

  public void AddSkin(string name, DialogSkin skin)
  {
    Skins.Add(name, skin);
  }

  public void SetSkin(string name)
  {
    if (Skins.ContainsKey(name))
      CurrentSkin = Skins[name];
    else
      Debug.LogError("No skin with name " + name + "!");
  }

  public void SetSkin(DialogSkin skin)
  {
    if (Skins.ContainsValue(skin))
      CurrentSkin = skin;
    else
    {
      AddSkin(skin.SkinName, skin);
      CurrentSkin = skin;
    }
  }
}
