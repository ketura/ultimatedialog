﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public interface IDialogListener
{
  void OnDialogClose(DialogResponse response, string data);
}

public enum DialogType { None, Dialog, Load, Save }

public class DialogController : Singleton<DialogController> 
{
  public GameObject DialogCanvasPrefab;

  public DialogSkinManager Manager;

  protected DialogBox DialogPrefab;
  protected FileDialog LoadPrefab;
  //protected SaveDialog DialogPrefab;

  public DialogResponse DialogResponse { get; protected set; }
  public DialogResponse LoadResponse { get; protected set; }
  public string PathResponse { get; protected set; }
  //public string SaveResponse { get; private set; }

  protected Camera DialogCamera;
  protected Canvas DialogCanvas;

  public DialogBox CurrentDialog { get; protected set; }
  public FileDialog CurrentLoad { get; protected set; }
  //protected SaveDialog CurrentSave;


  protected List<IDialogListener> LoadListeners;
  protected List<IDialogListener> SaveListeners;
  protected Dictionary<DialogType, List<IDialogListener>> DialogListeners;

  protected string[] drives;
  protected DriveFetcher fetcher;

  void Awake()
  {
    DialogListeners = new Dictionary<DialogType, List<IDialogListener>>();
    //UnitySystemConsoleRedirector.Redirect();

    StartCoroutine(PrimeDrives());
  }

  void Start () 
  {
    Debug.Log("Start");
    DialogCamera = Instantiate<GameObject>(DialogCanvasPrefab).GetComponent<Camera>();
    DialogCanvas = DialogCamera.GetComponentInChildren<Canvas>();

    if(Manager == null)
      Manager = DialogCanvas.GetComponent<DialogSkinManager>();
    DialogSkin skin = Manager.CurrentSkin;

    DialogPrefab = skin.DialogPrefab;
    LoadPrefab = skin.LoadPrefab;  
  }
  
  void Update () 
  {
    //Debug.Log("Update!");
  }

  IEnumerator PrimeDrives()
  {
    fetcher = new DriveFetcher();
    Debug.Log("Starting fetch thread.");
    fetcher.Start();
    Debug.Log("Looping fetch thread.");

    yield return StartCoroutine(fetcher.WaitFor());
    drives = fetcher.drives;
    fetcher = null;
  }

  public string[] GetDrives()
  {
    if(drives == null)
    {
      StopCoroutine(PrimeDrives());
      while (!fetcher.Update())
        continue;
      drives = fetcher.drives;
      fetcher = null;
    }

    return drives;
  }

  public void LoadDialog(string title)
  {
    StartCoroutine(AwaitFileDialog(LoadPrefab, title, Application.dataPath));
  }

  public void LoadDialog(string title, string path)
  {
    StartCoroutine(AwaitFileDialog(LoadPrefab, title, path));
  }

  public IEnumerator AwaitLoadDialog(string title, string path)
  {
    yield return StartCoroutine(AwaitFileDialog(LoadPrefab, title, path));
  }

  public IEnumerator AwaitFileDialog(FileDialog prefab, string title, string path)
  {
    yield return null;
    if (CurrentLoad == null)
    {
      CurrentLoad = Instantiate<FileDialog>(LoadPrefab);
      CurrentLoad.transform.SetParent(DialogCanvas.transform, false);
    }
    else
    {
      LoadResponse = DialogResponse.None;
      PathResponse = "";
      Debug.Log("Cannot display more than one load dialog at a time.");
      yield break;
    }

    CurrentLoad.SetTitle(title);

    while (CurrentLoad != null && CurrentLoad.CurrentResponse == DialogResponse.None)
      yield return null;

    DialogResponse = CurrentLoad.CurrentResponse;
    PathResponse = CurrentLoad.Pathname;
    CallListeners(DialogType.Load, DialogResponse, PathResponse);
    yield return null;
  }

  public void ShowDialog(string title, string message)
  {
    StartCoroutine(AwaitShowDialog(title, message, DialogMode.OK));
  }

  public void ShowDialog(DialogMode mode, string title, string message)
  {
    StartCoroutine(AwaitShowDialog(title, message, mode));
  }

  public void ShowCustomDialog(string title, string message, params string[] names)
  {
    StartCoroutine(AwaitShowCustomDialog(title, message, names));
  }

  public IEnumerator AwaitShowDialog(string title, string message, DialogMode mode = DialogMode.OK)
  {
    if (mode == DialogMode.OneButton || mode == DialogMode.TwoButton || mode == DialogMode.ThreeButton)
    {
      DialogResponse = DialogResponse.None;
      Debug.LogError("AwaitShowDialog cannot process OneButton, TwoButton, or ThreeButton modes.  Call AwaitShowCustomDialog instead.");
      yield break;
    }

    if (CurrentDialog == null)
    {
      CurrentDialog = Instantiate<DialogBox>(DialogPrefab);
      CurrentDialog.transform.SetParent(DialogCanvas.transform, false);
      CurrentDialog.text = message;
    }
    else
    {
      //This is going to need to change.
      DialogResponse = DialogResponse.None;
      Debug.Log("Cannot display more than one dialog at a time.");
      yield break;
    }

    CurrentDialog.SetMode(mode);
    CurrentDialog.SetTitle(title);

    while (CurrentDialog != null && CurrentDialog.CurrentResponse == DialogResponse.None)
      yield return null;

    DialogResponse = CurrentDialog.CurrentResponse;
    CallListeners(DialogType.Dialog, DialogResponse, "");
    yield return null;
  }

  public IEnumerator AwaitShowCustomDialog(string title, string message, params string[] names)
  {
    
    if (names.Length == 0)
    {
      DialogResponse = DialogResponse.None;
      Debug.LogError("AwaitShowCustomDialog must have customized button names passed to it.  If this is not needed, use AwaitShowDialog instead.");
      yield break;
    }

    CurrentDialog = Instantiate<DialogBox>(DialogPrefab);
    CurrentDialog.transform.SetParent(DialogCanvas.transform, false);
    CurrentDialog.text = message;
    CurrentDialog.SetTitle(title);

    switch (names.Length)
    {
      case 1:
        CurrentDialog.SetMode(DialogMode.OneButton, names);
        break;

      case 2:
        CurrentDialog.SetMode(DialogMode.TwoButton, names);
        break;

      default:
      case 3:
        CurrentDialog.SetMode(DialogMode.ThreeButton, names);
        break;
    }

    while (CurrentDialog != null && CurrentDialog.CurrentResponse == DialogResponse.None)
      yield return null;

    DialogResponse = CurrentDialog.CurrentResponse;
    CallListeners(DialogType.Dialog, DialogResponse, "");
    yield return null;
  }

  protected void CallListeners(DialogType type, DialogResponse response, string path)
  {
    if (DialogListeners.ContainsKey(type))
      foreach (IDialogListener listener in DialogListeners[type])
        listener.OnDialogClose(response, path);
  }

  public void HideCurrentDialog(float delay=0)
  {
    HideDialog(CurrentDialog, delay);
  }

  public void HideDialog(DialogBox dialog, float delay=0)
  {
    StartCoroutine(DelayHide(dialog, delay));
  }

  IEnumerator DelayHide(DialogBox dialog, float time)
  {
    yield return new WaitForSeconds(time);
    dialog.gameObject.SetActive(false);
    Destroy(dialog.gameObject);
    CurrentDialog = null;
  }

  public void AddListener(DialogType type, IDialogListener listener)
  {
    if (listener == null)
      return;

    if (!DialogListeners.ContainsKey(type))
      DialogListeners.Add(type, new List<IDialogListener>());

    DialogListeners[type].Add(listener);
  }

  public void RemoveListener(DialogType type, IDialogListener listener)
  {
    if (DialogListeners.ContainsKey(type))
      DialogListeners[type].Remove(listener);
  }

}
