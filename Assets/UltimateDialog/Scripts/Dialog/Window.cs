﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class Window : MonoBehaviour, IDragHandler
{
  public Button CloseButton;
  public RectTransform ContentPanel;
  public Text TitleText;
  public bool Resizable;

  protected virtual void Start () 
  {
    CloseButton.onClick.AddListener(delegate { this.CloseClicked(); });
  }
  
  void Update () 
  {
  
  }

  public virtual void CloseClicked()
  {
    this.gameObject.SetActive(false);
  }

  public void OnDrag(PointerEventData eventData)
  {
    ((RectTransform)transform).localPosition += new Vector3(eventData.delta.x, eventData.delta.y);
  }
}
