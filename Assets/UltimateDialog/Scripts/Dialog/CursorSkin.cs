﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum CursorType { None, Standard, ResizeVert, ResizeHoriz, ResizeNE, ResizeNW}

public class CursorSkin : MonoBehaviour 
{
  public string SkinName;

  public Texture2D BasicCursor;
  public Texture2D ResizeVertical;
  public Texture2D ResizeHorizontal;
  public Texture2D ResizeNE;
  public Texture2D ResizeNW;
}
