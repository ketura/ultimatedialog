﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(ColorTheme))]
public class DialogSkin : MonoBehaviour 
{
  public string SkinName;
  public DialogBox DialogPrefab;
  public FileDialog SavePrefab;
  public FileDialog LoadPrefab;

  public ColorTheme Theme;

  [ExecuteInEditMode]
  void Awake()
  {
    Theme = GetComponent<ColorTheme>();
  }
}
