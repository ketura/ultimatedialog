﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class DriveFetcher : ThreadedJob
{
  public string[] drives;
  public string[] fakeDrives;
  protected override void ThreadFunction()
  {
    string[] dr = Environment.GetLogicalDrives();
    List<string> realDrives = new List<string>();
    List<string> fakes = new List<string>();

    foreach (string d in dr)
      if (Directory.Exists(d))
        realDrives.Add(d);
      else
        fakes.Add(d);

    drives = realDrives.ToArray();
    fakeDrives = fakes.ToArray();
  }

  protected override void OnFinished()
  {
    base.OnFinished();
    Debug.Log("Thread finished");
    

    foreach (string s in drives)
      Debug.Log(s + " exists.");

    foreach(string s in fakeDrives)
      Debug.Log(s + " does not exist.");
  }
}

public class DirectoryFetcher : ThreadedJob
{
  public string path;
  public DirectoryInfo[] Directories;

  protected override void ThreadFunction()
  {
    DirectoryInfo[] subDirs = null;

    try
    {
      if (Directory.Exists(path))
      {
        DirectoryInfo info = new DirectoryInfo(path);
        subDirs = info.GetDirectories();
      }
      else
        error = "Path " + path + " does not exist!";
    }
    catch (UnauthorizedAccessException e) { error = "Ignoring " + path + ": " + e.Message; }
    catch (Exception e) { error = "" + e.GetType(); }

    Directories = subDirs;
  }
}

public class FileFetcher : ThreadedJob
{
  public string path;
  public FileInfo[] Files;

  protected override void ThreadFunction()
  {
    FileInfo[] files = null;
    try
    {
      if (Directory.Exists(path))
      {
        DirectoryInfo info = new DirectoryInfo(path);
        files = info.GetFiles();
      }
      else
        error = "Path " + path + " does not exist!";
    }
    catch (Exception e) { error = e.Message; }

    Files = files;
  }
}

public class FileDialog : DialogBox 
{
  public TreeView Explorer;
  //make this a property
  public string Pathname;

  protected Dictionary<string, DirectoryFetcher> DriveThreads;

  protected override void Start () 
  {
    base.Start();
    Debug.Log("Load start");

    //Remove this
    if (Pathname == "")
      Pathname = Application.dataPath;
    CurrentMode = DialogMode.OKCancel;
    StartCoroutine(LoadInitial());
  }

	void Update () 
  {
	
	}

  public override void Button1Clicked()
  {
    base.Button1Clicked();
  }

  public override void Button2Clicked()
  {
    Pathname = "asdf";
    base.Button2Clicked();
  }

  IEnumerator LoadInitial()
  {
    string[] path = SplitPath(Pathname);
    string currentPath = path[0];
    string[] drives =  DialogController.Instance.GetDrives();
    DriveThreads = new Dictionary<string, DirectoryFetcher>();
    foreach (string dr in drives)
    {
      Explorer.AddChild(SplitPath(dr)[0]);
      DriveThreads.Add(SplitPath(dr)[0], null);
    }

    yield return null;

    foreach (string dr in drives)
      StartCoroutine(LoadDirectory(dr, 2));

    yield return null;

    string part = "";
    for (int i = 0; i < path.Length; ++i)
    {
      yield return null;
      part += path[i] + Path.DirectorySeparatorChar;
      StartCoroutine(LoadDirectory(part));
    }

    Explorer.SortChildren();

  }

  IEnumerator LoadDirectory(string path, int iterations=0)
  {
    //Debug.Log(iterations + " Path: " + path);
    List<DirectoryInfo> subDirs = null;

    string root = SplitPath(Path.GetPathRoot(path))[0];
    //Debug.Log("Fetching " + root);
    DirectoryFetcher thread = DriveThreads[root];
    if (thread != null)
    {
      while (DriveThreads[root] != null)
        yield return null;
    }
    thread = new DirectoryFetcher();
    thread.path = path;
    thread.Start();
    yield return StartCoroutine(thread.WaitFor());

    if (thread.Directories == null)
    {
      yield break;
    }
    else
    {
      subDirs = new List<DirectoryInfo>(thread.Directories);
      thread = null;
    }

    foreach (DirectoryInfo dirInfo in subDirs)
    {
      Explorer.AddChild(dirInfo.FullName.Replace('\\', '/'));
      yield return null;
    }
    yield return null;

    if(iterations != 0)
    {
      foreach (DirectoryInfo dirInfo in subDirs)
        StartCoroutine(LoadDirectory(dirInfo.FullName, iterations - 1));
    }
  }

  public void LoadPath(string path)
  {
    StartCoroutine(LoadDirectory(path, 1));
  }

  public string[] SplitPath(string s)
  {
    return s.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
  }


}
