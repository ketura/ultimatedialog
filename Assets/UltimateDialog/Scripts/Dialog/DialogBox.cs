﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public enum DialogMode { OK, OKCancel, YesNo, YesNoCancel, OneButton, TwoButton, ThreeButton };
public enum DialogResponse { None, OK, Cancel, Yes, No, Button1, Button2, Button3 };

public class DialogBox : Window 
{
  public Text MessageText;
  public virtual string text
  {
    get
    {
      return MessageText.text;
    }

    set
    {
      MessageText.text = value;
    }
  }

  public DialogResponse CurrentResponse { get; private set; }

  public Button Button1;
  public Button Button2;
  public Button Button3;

  public Button DefaultButton;

  protected DialogMode CurrentMode;

  protected override void Start () 
  {
    base.Start();
    Debug.Log("Dialog start");
    if(Button1 != null)
      Button1.onClick.AddListener(delegate { this.Button1Clicked(); });
    if (Button2 != null)
      Button2.onClick.AddListener(delegate { this.Button2Clicked(); });
    if (Button3 != null)
      Button3.onClick.AddListener(delegate { this.Button3Clicked(); });

    CurrentResponse = DialogResponse.None;
  }
  
  void Update () 
  {
  
  }

  protected void SelectButton(Button btn)
  {
    EventSystem.current.SetSelectedGameObject(btn.gameObject);
  }

  public override void CloseClicked()
  {
    SetResponse(DialogResponse.Button1);
    DialogController.Instance.HideDialog(this);
  }

  public void SetTitle(string title)
  {
    TitleText.text = title;
  }

  public virtual void SetMode(DialogMode mode, params string[] names)
  {
    CurrentMode = mode;
    switch(mode)
    {
      case DialogMode.OKCancel:
        Button3.gameObject.SetActive(false);
        Button2.gameObject.SetActive(true);
        Button2.GetComponentInChildren<Text>().text = "OK";
        Button1.GetComponentInChildren<Text>().text = "Cancel";
        SelectButton(Button2);
        break;
      case DialogMode.YesNo:
        Button3.gameObject.SetActive(false);
        Button2.gameObject.SetActive(true);
        Button2.GetComponentInChildren<Text>().text = "Yes";
        Button1.GetComponentInChildren<Text>().text = "No";
        SelectButton(Button2);
        break;
      case DialogMode.YesNoCancel:
        Button3.gameObject.SetActive(true);
        Button3.GetComponentInChildren<Text>().text = "Yes";
        Button2.gameObject.SetActive(true);
        Button2.GetComponentInChildren<Text>().text = "No";
        Button1.GetComponentInChildren<Text>().text = "Cancel";
        SelectButton(Button3);
        break;
      case DialogMode.OneButton:
        if (names.Length < 1)
          Debug.LogError("Need a custom button name for OneButton mode!");
        Button2.gameObject.SetActive(false);
        Button3.gameObject.SetActive(false);
        Button1.GetComponentInChildren<Text>().text = names[0];
        SelectButton(Button1);
        break;
      case DialogMode.TwoButton:
        if (names.Length < 2)
          Debug.LogError("Need 2 custom button names for TwoButton mode!");
        Button3.gameObject.SetActive(false);
        Button2.gameObject.SetActive(true);
        Button2.GetComponentInChildren<Text>().text = names[0];
        Button1.GetComponentInChildren<Text>().text = names[1];
        SelectButton(Button2);
        break;
      case DialogMode.ThreeButton:
        if (names.Length < 3)
          Debug.LogError("Need 3 custom button names for ThreeButton mode!");
        Button3.gameObject.SetActive(true);
        Button3.GetComponentInChildren<Text>().text = names[0];
        Button2.gameObject.SetActive(true);
        Button2.GetComponentInChildren<Text>().text = names[1];
        Button1.GetComponentInChildren<Text>().text = names[2];
        SelectButton(Button3);
        break;
      default:
      case DialogMode.OK:
        Button2.gameObject.SetActive(false);
        Button3.gameObject.SetActive(false);
        Button1.GetComponentInChildren<Text>().text = "OK";
        SelectButton(Button1);
        break;
    }
  }

  protected void SetResponse(DialogResponse button)
  {
    if(CurrentMode == DialogMode.OneButton || CurrentMode == DialogMode.TwoButton || CurrentMode == DialogMode.ThreeButton)
    {
      CurrentResponse = button;
      return;
    }

    switch (button)
    {
      case DialogResponse.Button1:
        switch(CurrentMode)
        {
          case DialogMode.OKCancel:
          case DialogMode.YesNoCancel:
            CurrentResponse = DialogResponse.Cancel;
            break;
          case DialogMode.YesNo:
            CurrentResponse = DialogResponse.No;
            break;
          case DialogMode.OK:
            CurrentResponse = DialogResponse.OK;
            break;
        }
        break;

      case DialogResponse.Button2:
        switch (CurrentMode)
        {
          case DialogMode.OKCancel:
            CurrentResponse = DialogResponse.OK;
            break;
          case DialogMode.YesNoCancel:
            CurrentResponse = DialogResponse.No;
            break;
          case DialogMode.YesNo:
            CurrentResponse = DialogResponse.Yes;
            break;
        }
        break;

      case DialogResponse.Button3:
        switch (CurrentMode)
        {
          case DialogMode.YesNoCancel:
            CurrentResponse = DialogResponse.Yes;
            break;
        }
        break;
    }


  }

  public virtual void Button1Clicked()
  {
    Debug.Log("Button 1 Clicked.");
    SetResponse(DialogResponse.Button1);
    DialogController.Instance.HideDialog(this);
  }

  public virtual void Button2Clicked()
  {
    Debug.Log("Button 2 Clicked.");
    SetResponse(DialogResponse.Button2);
    DialogController.Instance.HideDialog(this);
  }

  public virtual void Button3Clicked()
  {
    Debug.Log("Button 3 Clicked.");
    SetResponse(DialogResponse.Button3);
    DialogController.Instance.HideDialog(this);
  }
}
