﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Tester : MonoBehaviour, IDialogListener
{

  void Start () 
  {
    DialogController.Instance.AddListener(DialogType.Dialog, this);
    DialogController.Instance.AddListener(DialogType.Load, this);
  }
  
  void Update () 
  {
  
  }

  public void LoadEverything()
  {
    DialogController.Instance.LoadDialog("LOADING WHAOW!");
  }

  public void DestroyEverything()
  {
    //DialogController.Instance.ShowDialog("Boom!", "Mwahaha, everything is now destroyed!");
    //bool result = DialogController.Instance.ShowDialog("Really quit?", "Do you really want to quit?");
    StartCoroutine(CoroutineTest());
  }

  public void DestroyEverythingProperly()
  {
    DialogController.Instance.ShowDialog("BOOM!", "You just destroyed everything, sicko.");
  }

  IEnumerator CoroutineTest()
  {
    yield return StartCoroutine(DialogController.Instance.AwaitShowDialog("Booom!", "Wow, it really is all just gone."));
    Debug.Log(DialogController.Instance.DialogResponse);

    yield return StartCoroutine(DialogController.Instance.AwaitShowDialog("Innit?", "This is pretty great, right?", DialogMode.YesNo));
    Debug.Log(DialogController.Instance.DialogResponse);

    yield return StartCoroutine(DialogController.Instance.AwaitShowDialog("What!", "Say yes or you're dead.", DialogMode.YesNoCancel));
    Debug.Log(DialogController.Instance.DialogResponse);

    yield return StartCoroutine(DialogController.Instance.AwaitShowDialog("Poo.", "You're no fun.", DialogMode.OKCancel));
    Debug.Log(DialogController.Instance.DialogResponse);

    yield return StartCoroutine(DialogController.Instance.AwaitShowCustomDialog("Dude.", "You suck.", "Screw", "You", "Dude"));
    Debug.Log(DialogController.Instance.DialogResponse);
  }

  public void OnDialogClose(DialogResponse response, string data)
  {
    Debug.Log("Here's the response: " + response + "; " + data);
  }

  public void LoadDialog()
  {
    DialogController.Instance.LoadDialog("LOADING, BITCH", Application.dataPath);
  }
}
