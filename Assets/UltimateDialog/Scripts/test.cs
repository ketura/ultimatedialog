﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(DoubleClickHandler))]
public class test : MonoBehaviour, IDoubleClickHandler
{
  private DoubleClickHandler _clicker;

  void Start () 
  {
    _clicker = GetComponent<DoubleClickHandler>();
    _clicker.Handler = this;
  }

  public void OnPointerClick()
  {

  }

  public void OnPointerDoubleClick()
  {
    Debug.Log("Double!");
  }
}
