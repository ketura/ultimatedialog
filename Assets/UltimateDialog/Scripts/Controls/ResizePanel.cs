﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using RektTransform;

public class ResizePanel : MonoBehaviour 
{
  //Panel1 should be left or top.
  public PanelView Panel1;
  public PanelView Panel2;


  public Slider Divider;
  public float Padding;

  [Range(0.05f, 0.5f)]
  public float Panel1MinSize;
  [Range(0.05f, 0.5f)]
  public float Panel2MinSize;

  private RectTransform RT;
  private RectTransform Div;
  private RectTransform Panel1RT;
  private RectTransform Panel2RT;
  private float DivPos;

  [ExecuteInEditMode]
	void Awake () 
  {
    RT = this.RT();
    Div = Divider.RT();
    Panel1RT = Panel1.RT();
    Panel2RT = Panel2.RT();
  }

  [ExecuteInEditMode]
  void Start()
  {
    ResizeDivider();
    UpdateValue();
  }

  [ExecuteInEditMode]
	void Update () 
  {
    if (RT.hasChanged || DivPos != Divider.value)
    {
      ResizeDivider();
      UpdateValue();
    }
  }

  public void ResizeDivider()
  {
    RT.hasChanged = false;

    Divider.maxValue = 1.0f - Panel2MinSize;
    Divider.minValue = Panel1MinSize;

    Div.MoveLeftInside(Panel1MinSize * (RT.rect.width - Padding) + Padding / 2);
    Div.Right( Panel2MinSize * (RT.rect.width - Padding) + Padding/2);
  }

  public void UpdateValue()
  {
    DivPos = Divider.value;

    Panel1RT.SetWidth((RT.rect.width - Padding) * DivPos);
    Panel1RT.MoveLeftInside();

    Panel2RT.SetWidth((RT.rect.width - Padding) * (1-DivPos));
    Panel2RT.MoveRightInside();
  }
}
