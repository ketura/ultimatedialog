﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(InputField))]
public class TextDropDown : DropDownList, IPointerClickHandler
{
  public override string CurrentText
  {
    get
    {
      if (CurrentItem == NullItem && Input.placeholder != null && EventSystem.current.currentSelectedGameObject != this.gameObject)
          return ((Text)Input.placeholder).text;
      //Debug.Log((CurrentItem == NullItem) + ", " + (Input.placeholder != null) + ", " + (EventSystem.current.currentSelectedGameObject == this.gameObject));
      return Input.text;
    }
    set
    {
      if (CurrentItem == NullItem && Input.placeholder != null)
        ((Text)Input.placeholder).text = value;
      else
        Input.text = value;
    }
  }
  public RectTransform Caret;
  public Vector2 CaretOffset;
  public bool EnableAtStart;
  public InputField Input;

  private bool _enabled;
  public bool InputEnabled
  {
    get
    {
      return _enabled;
    }

    set
    {
      _enabled = value;
      Input.enabled = _enabled;
    }
  }

  private bool _updating;

	new void Awake()
  {
    base.Awake();
    if(Input == null)
      Input = GetComponent<InputField>();
    InputEnabled = EnableAtStart;
    
	}

	void Update () 
  {
	}

  public void EndEdit()
  {
    if (InputEnabled && CurrentText != "")
    {
      if (!Items.ContainsKey(CurrentText))
      {
        AddListItem(CurrentText);
        SetCurrentItem(CurrentText);
      }
    }
    else if(CurrentItem != NullItem)
      CurrentText = CurrentItem.text;

  }

  public new void OnPointerClick(PointerEventData eventData)
  {
    //Debug.Log("click");
    //Caret = transform.FindChild("Input Caret").GetComponent<RectTransform>();
    //Debug.Log(Caret);

    if (CurrentText == DefaultText)
      CurrentText = "";

    if(Items.Count > 0)
      ToggleShow();
  }


}
