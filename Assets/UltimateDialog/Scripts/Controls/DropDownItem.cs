﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(LayoutElement))]
public class DropDownItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IDragHandler
{
  public Text Label;
  public float TextOffset = 5;

  public Color TextColor = Color.black;
  public Color HighlightedColor = Color.white;

  public string text
  {
    get
    {
      return Label.text;
    }

    set
    {
      Label.text = value;
    }
  }

  [HideInInspector]
  public DropDownList parent;

  private LayoutElement _le;

  [ExecuteInEditMode]
  void Awake () 
  {
    if (Label == null)
      Label = GetComponentInChildren<Text>();
    Label.color = Color.black;
    _le = GetComponent<LayoutElement>();
    _le.preferredWidth =  Label.preferredWidth + TextOffset;
  }

  [ExecuteInEditMode]
  void Update()
  {
    _le.preferredWidth = Label.preferredWidth + TextOffset;
  }

  public void SetHover(bool hover)
  {
    if (hover)
      Label.color = HighlightedColor;
    else
      Label.color = TextColor;
  }

  public void OnPointerEnter(PointerEventData eventData)
  {
    Label.color = HighlightedColor;
  }

  public void OnPointerExit(PointerEventData eventData)
  {
    Label.color = TextColor;
  }

  //This is used instead of the Inspector OnClick to facilitate programmatically generated items.
  public void OnPointerClick(PointerEventData eventData)
  {
    if (parent != null)
      parent.ChildClicked(this);
  }

  public void OnDrag(PointerEventData eventData)
  {
    Label.color = TextColor;
  }
}
