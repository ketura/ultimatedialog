﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

//[CustomEditor(typeof(ResizePanel))]
public class ResizePanelEditor : Editor
{

  public override void OnInspectorGUI()
  {
    var resize = (ResizePanel)target;

    //resize.Panel1 = (RectTransform)EditorGUILayout.ObjectField("Input Field", resize.Panel1, typeof(RectTransform), true);
    //resize.Panel2 = (RectTransform)EditorGUILayout.ObjectField("Input Field", resize.Panel2, typeof(RectTransform), true);

    resize.Divider = (Slider)EditorGUILayout.ObjectField("Input Field", resize.Divider, typeof(Slider), true);


  //public float Panel1MinSize;
  //public float Panel2MinSize;

  //public RectTransform RT;


  //combo.Input = (InputField)EditorGUILayout.ObjectField("Input Field", combo.Input, typeof(InputField), true);

    
  }
}
