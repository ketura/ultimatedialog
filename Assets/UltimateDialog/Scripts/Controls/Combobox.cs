﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class Combobox : MonoBehaviour 
{
  public InputField Input;

  public Dropdown Dropdown;
  public Button ArrowButton;

  void Start () 
  {
    ArrowButton.onClick.AddListener(ButtonClicked);
    Input.onEndEdit.AddListener(delegate { this.LockInput(); });
    Dropdown.onValueChanged.AddListener(delegate { this.OptionPicked(); });
  }
  
  void Update () 
  {

  }

  public void DropdownClicked()
  {
    //Debug.Log("Dropdown clicked");
    EventSystem.current.SetSelectedGameObject(Input.gameObject);
  }

  public void LockInput()
  {
    Debug.Log("Locked");
    string CurrentText = Input.text;
    if (CurrentText == "")
      return;

    List<Dropdown.OptionData> Options = Dropdown.options;

    if(!OptionExists(CurrentText))
    {
      Dropdown.options.Add(new Dropdown.OptionData(CurrentText));
    }
  }

  public void ButtonClicked()
  {
    Debug.Log("Clicked");
    ExecuteEvents.Execute(Dropdown.gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.pointerClickHandler);
  }

  public void OptionPicked()
  {
    Debug.Log("Current Option: " + GetCurrentOption());
    Input.text = GetCurrentOption();
  }

  public bool OptionExists(string item)
  {
    List<Dropdown.OptionData> Options = Dropdown.options;
    for (int i = 0; i < Options.Count; ++i)
    {
      Dropdown.OptionData child = Options[i];
      if (child.text == item)
        return true;
    }

    return false;
  }

  public string GetOption(int index)
  {
    return Dropdown.options[index].text;
  }

  public string GetCurrentOption()
  {
    return Dropdown.options[Dropdown.value].text;
  }

  public bool IsPanelVisible()
  {
    Transform child = Dropdown.transform.Find("Dropdown List");
    return child != null;
  }

  
}
