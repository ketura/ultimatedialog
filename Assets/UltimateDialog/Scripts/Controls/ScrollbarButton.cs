﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using RektTransform;


public enum Direction { Positive, Negative };

[RequireComponent(typeof(Button))]
public class ScrollbarButton : MonoBehaviour 
{
  public Scrollbar ParentScroll;
  public RectTransform.Axis Orientation;
  public Direction Dir;
  [Range(0.01f, 1.0f)]
  public float Step;

  private Button button;

  [ExecuteInEditMode]
  void Awake () 
  {
    button = GetComponent<Button>();

    if (ParentScroll.direction == Scrollbar.Direction.LeftToRight || ParentScroll.direction == Scrollbar.Direction.RightToLeft)
      Orientation = RectTransform.Axis.Horizontal;
    else
      Orientation = RectTransform.Axis.Vertical;

    if (Orientation == RectTransform.Axis.Horizontal)
    {
      if (Dir == Direction.Positive)
        transform.localRotation = Quaternion.Euler(0, 0, -90);
      else
        transform.localRotation = Quaternion.Euler(0, 0, 90);
    }
    else
    {
      if (Dir == Direction.Positive)
        transform.localRotation = Quaternion.Euler(0, 0, 180);
      else
        transform.localRotation = Quaternion.Euler(0, 0, 0);
    }

    button.onClick.AddListener(delegate { this.OnClick(); });

  
  }

  [ExecuteInEditMode]
  void Update()
  {
    if (Orientation == RectTransform.Axis.Horizontal)
      button.RT().SetHeight(ParentScroll.RT().GetHeight());
    else
      button.RT().SetWidth(ParentScroll.RT().GetWidth());
  }

  public void OnClick()
  {
    if (Dir == Direction.Positive)
      ParentScroll.value = Mathf.Clamp01(ParentScroll.value + Step);
    else
      ParentScroll.value = Mathf.Clamp01(ParentScroll.value - Step);

  }

}
