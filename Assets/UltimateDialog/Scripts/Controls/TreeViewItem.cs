﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using RektTransform;
using System;

public class TreeViewItem : MonoBehaviour, IDoubleClickHandler
{
  public Button Highlight;
  public Toggle toggle;
  public GameObject ArrowParent;
  public GameObject UnselectedArrow;
  public GameObject SelectedArrow;
  public Text Label;
  public string FullName;

  public GameObject Controls;
  public GameObject Children;

  public int Level;

  [HideInInspector]
  public TreeView ParentTree;

  private DialogSkin CurrentSkin;
  private Dictionary<string, TreeViewItem> ChildItems;

  public bool Selected { get; private set; }

  public bool Toggled
  {
    get
    {
      return toggle.isOn;
    }

    set
    {
      toggle.isOn = value;
    }
  }

  public string text
  {
    get
    {
      return Label.text;
    }

    set
    {
      Label.text = value;
    }
  }

  void Awake () 
  {
    UpdateLevel(Level, 5);
    ChildItems = new Dictionary<string, TreeViewItem>();
    Selected = false;
  }

  void Start()
  {
    Highlight.GetComponent<DoubleClickHandler>().Handler = this;
    ArrowParent.SetActive(ChildItems.Count > 0);
    //CurrentSkin = DialogSkinManager.Instance.CurrentSkin;
    //Debug.Log(CurrentSkin);
  }
  
  void Update () 
  {
    
  }

  public void UpdateSkin(DialogSkin skin)
  {
    CurrentSkin = skin;
    if (Selected)
      Select();
    else
      Unselect();
  }

  public void UpdateLevel(int level, float indent)
  {
    Level = level;
    Controls.RT().SetLeft(indent * level);
  }

  public void OnToggleClick()
  {
    Children.SetActive(Toggled);
    SelectedArrow.SetActive(Toggled);
    UnselectedArrow.SetActive(!Toggled);
  }

  public void Toggle()
  {
    Toggled = !Toggled;
    OnToggleClick();

    if (Toggled)
      ParentTree.OpenItem(this);
  }

  public void AddChild(TreeViewItem item)
  {
    if (!ChildItems.ContainsKey(item.text))
    {
      ChildItems.Add(item.text, item);
      item.transform.SetParent(Children.transform, false);
    }

    ArrowParent.SetActive(ChildItems.Count > 0);
    OnToggleClick();
  }

  public void RemoveChild(TreeViewItem item)
  {
    RemoveChild(item.text);
  }

  public void RemoveChild(string name)
  {
    if (ChildItems.ContainsKey(name))
    {
      ChildItems.Remove(name);
    }

    ArrowParent.SetActive(ChildItems.Count > 0);
    OnToggleClick();
  }

  public TreeViewItem FindChild(string name)
  {
    if (!ChildItems.ContainsKey(name))
      return null;

    return ChildItems[name];
  }

  public void SortChildren()
  {
    List<string> childs = new List<string>();
    foreach (string s in ChildItems.Keys)
      childs.Add(s);

    string[] sorted = childs.ToArray();
    System.Array.Sort(sorted, System.StringComparer.InvariantCulture);

    for (int i = 0; i < sorted.Length; ++i)
    {
      string key = sorted[i];
      ChildItems[key].transform.SetSiblingIndex(i);
    }
  }

  public void OnPointerClick()
  {
    ParentTree.RequestSelection(this);
  }

  public void OnPointerDoubleClick()
  {
    Toggle();
  }

  public void Select()
  {
    Selected = true;
    Highlight.colors = CurrentSkin.Theme.TreeSelectBlock;
  }

  public void Unselect()
  {
    Selected = false;
    Highlight.colors = CurrentSkin.Theme.TreeHighlightBlock;
  }
}
