﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using RektTransform;

[RequireComponent(typeof(RectTransform))]
[ExecuteInEditMode]
public class FullScrollbar : MonoBehaviour 
{
  public Button PosButton;
  public Button NegButton;
  public Scrollbar scrollbar;

  public Scrollbar.Direction direction;
  [Range(0.01f, 1.0f)]
  public float Step = 0.1f;
  [Range(0.0f, 1.0f)]
  public float DefaultValue;

  public float Long;
  public float Skinny;
  private Scrollbar.Direction LastDir;
  private RectTransform RT;

  private RectTransform PosRT;
  private RectTransform NegRT;

  void Start()
  {
    PosButton.onClick.AddListener(delegate { this.TopClicked(); });
    NegButton.onClick.AddListener(delegate { this.BottomClicked(); });
    scrollbar.value = DefaultValue;
  }

  void Awake()
  {
    PosRT = PosButton.RT();
    NegRT = NegButton.RT();

    RT = this.RT();
    LastDir = scrollbar.direction;
    //Debug.Log("Awake: " + this + ", " + RT);
    Resize();
  }


  void Update()
  {

  }

  public  void CheckSize()
  {
    float newLong = 0;
    float newSkinny = 0;
    if (IsVertical(direction))
    {
      newLong = RT.rect.height;
      newSkinny = RT.rect.width;
    }
    else
    {
      newLong = RT.rect.width;
      newSkinny = RT.rect.height;
    }
    
    if (newLong != Long || newSkinny != Skinny)
    {
      Debug.Log(this + " changes: " + Long + " to " + newLong + ", " + Skinny + " to " + newSkinny);
      Long = newLong;
      Skinny = newSkinny;
      Resize();
    }
  }

  public bool ScrollActive()
  {
    return scrollbar.gameObject.activeSelf;
  }

  public bool IsVertical(Scrollbar.Direction dir)
  {
    return dir == Scrollbar.Direction.BottomToTop || dir == Scrollbar.Direction.TopToBottom;
  }

  public bool IsHorizontal(Scrollbar.Direction dir)
  {
    return dir == Scrollbar.Direction.LeftToRight || dir == Scrollbar.Direction.RightToLeft;
  }

  public void Resize()
  {
    //Debug.Log("Resizing: " + this);
    scrollbar.direction = direction;

    if (IsVertical(direction))
    {
      if (direction != LastDir && IsHorizontal(LastDir))
        RT.SetSize(Skinny, Long);
      else
        scrollbar.RT().SetSize(Skinny, Long - Skinny * 2);
    }
    else
    {
      if (direction != LastDir && IsVertical(LastDir))
        RT.SetSize(Long, Skinny);
      else
        scrollbar.RT().SetSize(Long - Skinny * 2, Skinny);
    }

    if (direction != LastDir)
    {
      PosRT.SetSize(Skinny, Skinny);
      NegRT.SetSize(Skinny, Skinny);
      switch (direction)
      {
        case Scrollbar.Direction.BottomToTop:
          PosRT.localRotation = Quaternion.Euler(0, 0, 0);
          PosRT.SetAnchors(Anchors.TopCenter);
          PosRT.MoveTopOutside();
          PosRT.MoveLeftInside();

          NegRT.localRotation = Quaternion.Euler(0, 0, 180);
          NegRT.SetAnchors(Anchors.BottomCenter);
          NegRT.MoveBottomOutside();
          NegRT.MoveLeftInside();
          break;
        case Scrollbar.Direction.TopToBottom:
          PosRT.localRotation = Quaternion.Euler(0, 0, 180);
          PosRT.SetAnchors(Anchors.BottomCenter);
          PosRT.MoveBottomOutside();
          PosRT.MoveLeftInside();

          NegRT.localRotation = Quaternion.Euler(0, 0, 0);
          NegRT.SetAnchors(Anchors.TopCenter);
          NegRT.MoveTopOutside();
          NegRT.MoveLeftInside();
          break;
        case Scrollbar.Direction.RightToLeft:
          PosRT.localRotation = Quaternion.Euler(0, 0, 90);
          PosRT.SetAnchors(Anchors.MiddleLeft);
          PosRT.MoveLeftOutside();
          PosRT.MoveTopInside();

          NegRT.localRotation = Quaternion.Euler(0, 0, -90);
          NegRT.SetAnchors(Anchors.MiddleRight);
          NegRT.MoveRightOutside();
          NegRT.MoveTopInside();
          break;
        case Scrollbar.Direction.LeftToRight:
          PosRT.localRotation = Quaternion.Euler(0, 0, -90);
          PosRT.SetAnchors(Anchors.MiddleRight);
          PosRT.MoveRightOutside();
          PosRT.MoveTopInside();

          NegRT.localRotation = Quaternion.Euler(0, 0, 90);
          NegRT.SetAnchors(Anchors.MiddleLeft);
          NegRT.MoveLeftOutside();
          NegRT.MoveTopInside();
          break;
      }
    }

    LastDir = direction;
  }

  public void TopClicked()
  {
    scrollbar.value = Mathf.Clamp01(scrollbar.value + Step);
  }

  public void BottomClicked()
  {
    scrollbar.value = Mathf.Clamp01(scrollbar.value - Step);
  }
}
