﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using RektTransform;

public class PanelView : MonoBehaviour 
{
  public FullScrollbar verticalScrollbar;
  public FullScrollbar horizontalScrollbar;

  public RectTransform Corner;

  private RectTransform horiz;
  private RectTransform vert;

  

  void Start () 
  {
    if (horizontalScrollbar != null)
    {
      horiz = horizontalScrollbar.RT();
      horiz.MoveBottomInside();
    }

    if (verticalScrollbar != null)
    {
      vert = verticalScrollbar.RT();
      vert.MoveRightInside(0);
    }
    StartCoroutine(UpdateDelay());
  }

  IEnumerator UpdateDelay()
  {
    //Give the scroll rect a frame or so to figure out what it's doing with the scrollbars.
    yield return null;
    UpdateScrollbars();
  }
  
  void Update () 
  {
  
  }

  public void UpdateScrollbars()
  {
    if (horiz != null && vert != null)
    {
      if (vert != null && verticalScrollbar.gameObject.activeSelf)
        verticalScrollbar.Resize();
      if (horiz != null && horizontalScrollbar.gameObject.activeSelf)
        horizontalScrollbar.Resize();

      if ((horizontalScrollbar.scrollbar.gameObject.activeInHierarchy && horizontalScrollbar.scrollbar.gameObject .activeSelf)
        && (verticalScrollbar.scrollbar.gameObject.activeInHierarchy && verticalScrollbar.scrollbar.gameObject.activeSelf))
      {
        horiz.SetRight(vert.GetWidth());
        vert.SetBottom(horiz.GetHeight());

        Corner.SetWidth(vert.GetWidth());
        Corner.SetHeight(horiz.GetHeight());
        Corner.gameObject.SetActive(true);
      }
      else
      {
        //Debug.Log("Adjusting for one");
        horiz.SetRight(0);
        vert.SetBottom(0);
        Corner.gameObject.SetActive(false);
      }
    }
  }
}
