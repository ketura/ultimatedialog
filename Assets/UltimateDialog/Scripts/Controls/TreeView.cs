﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RektTransform;

public class TreeView : MonoBehaviour 
{
  public TreeViewItem ItemPrefab;
  public float Indent;

  public TreeViewItem SelectedItem { get; protected set; }

  private Dictionary<string, TreeViewItem> Children;

  private RectTransform RT;
  private float Y;
  private DialogSkin CurrentSkin;

  void Awake()
  {
    Children = new Dictionary<string, TreeViewItem>();
    foreach (Transform child in this.transform)
    {
      TreeViewItem item = child.GetComponent<TreeViewItem>();
      if (item != null)
        Children.Add(item.text, item);
    }

    RT = this.RT();
    Y = RT.GetTop().y;

    CurrentSkin = DialogSkinManager.Instance.CurrentSkin;
  }


  void Start () 
  {
    SortChildren();
    
  }
  
  void Update () 
  {
    
  }

  public void SortChildren()
  {
    List<string> childs = new List<string>();
    foreach (string s in Children.Keys)
      childs.Add(s);

    string[] sorted = childs.ToArray();
    System.Array.Sort(sorted, System.StringComparer.InvariantCulture);

    for(int i = 0; i < sorted.Length; ++i)
    {
      string key = sorted[i];
      Children[key].transform.SetSiblingIndex(i);
    }
  }

  public void AddChild(string label)
  {
    string[] levels = label.Split('/');

    TreeViewItem previousLevel = null;
    int currentLevel = 0;
    string currentName = "";

    for(int i = 0; i < levels.Length; ++i)
    {
      string name = levels[i];
      currentName += name + '/';
      //Debug.Log(name);
      if(name == "")
        continue;

      TreeViewItem treeitem = null;
      if (previousLevel == null)
      {
        if (Children.ContainsKey(name))
          treeitem = Children[name];
      }
      else
        treeitem = previousLevel.FindChild(name);

      if (treeitem == null)
      {
        treeitem = Instantiate<TreeViewItem>(ItemPrefab);
        treeitem.gameObject.name = name;
        treeitem.text = name;
        treeitem.ParentTree = this;
        treeitem.UpdateSkin(CurrentSkin);
        treeitem.FullName = currentName;

        if (previousLevel == null)
        {
          Children.Add(name, treeitem);
          treeitem.transform.SetParent(this.transform, false);
        }
        else
          previousLevel.AddChild(treeitem);
      }

      treeitem.UpdateLevel(currentLevel, Indent);

      previousLevel = treeitem;
      currentLevel++;
    }
  }

  public void RequestSelection(TreeViewItem item)
  {
    if(SelectedItem != null)
      SelectedItem.Unselect();
    SelectedItem = item;
    SelectedItem.Select();
  }

  public void OpenItem(TreeViewItem item)
  {
    Debug.Log(item.FullName);
    DialogController.Instance.CurrentLoad.LoadPath(item.FullName);
  }
}
