﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RektTransform;
using UnityEngine.EventSystems;

[RequireComponent(typeof(DetailCell))]
public class HeaderCell : MonoBehaviour, IPointerClickHandler
{
  public CellDivider Divider;
  public float MinWidth = 30f;

  [HideInInspector]
  public HeaderRow ParentRow;

  private RectTransform Div;
  protected DetailCell detail;
  protected RectTransform RT;
  void Awake()
  {
    RT = GetComponent<RectTransform>();
    detail = GetComponent<DetailCell>();
    Div = Divider.RT();
  }

  void Start()
  {
    Divider.Parent = this;
  }

  public void OnDividerDoubleClick()
  {
    detail.ParentRow.ParentView.FitWidth(detail.ID);
  }

  public void OnDividerMove()
  {
    RT.SetWidth(RT.GetWidth() + Div.anchoredPosition.x);
    if (RT.GetWidth() <= MinWidth)
      RT.SetWidth(MinWidth);
    Div.anchoredPosition = new Vector2(0, Div.anchoredPosition.y);
    RT.MoveLeft();

    ParentRow.DividerAdjust(detail.ID, RT.GetWidth());
  }

  public void OnPointerClick(PointerEventData eventData)
  {
    ParentRow.CellClicked(detail.ID);
  }

  public void SetHeader(bool active)
  {
    Divider.gameObject.SetActive(active);
    //Label.gameObject.SetActive(active);
  }
}
