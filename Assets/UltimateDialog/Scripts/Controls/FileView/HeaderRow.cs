﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RektTransform;


public class HeaderRow : DetailRow 
{
  public HeaderCell HeaderCellPrefab;

	new void Awake() 
  {
    Init();
	}

	void Update () 
  {
	
	}

  public void DividerAdjust(int column, float width)
  {
    ParentView.DividerAdjust(column, width);
    CheckWidth();
  }

  public override void CellClicked(int column)
  {
    ParentView.ColumnHeaderClicked(column);
  }

  private HeaderCell CreateCell(DetailCell CellPrefab=null)
  {
    HeaderCell head;
    if (HeaderCellPrefab != null)
    {
      head =  Instantiate<HeaderCell>(HeaderCellPrefab);
      head.ParentRow = this;
      return head;
    }

    DetailCell cell;
    
    GameObject go;

    if (CellPrefab != null)
    {
      cell = Instantiate<DetailCell>(CellPrefab);
      head = cell.gameObject.AddComponent<HeaderCell>();
      go = new GameObject("Divider");
      head.Divider = go.AddComponent<CellDivider>();
      head.ParentRow = this;
      go.AddComponent<DoubleClickHandler>();
      
      RectTransform div = go.AddComponent<RectTransform>();
      div.SetParent(head.transform, false);
      div.SetAnchors(Anchors.MiddleRight);
      div.MoveFrom(Anchors.MiddleRight, Vector2.zero);

      return head;
    }
    else
    {
      Debug.LogError("Both passed prefab and HeaderCellPrefab are null!  Cannot create cell from nothing.");
      return null;
    }
  }

  public override void CreateRow(DetailCell CellPrefab=null)
  {
    for (int i = 0; i < Count; ++i)
    {

      HeaderCell head = CreateCell(CellPrefab);
      DetailCell cell = head.GetComponent<DetailCell>();

      if (i == 0)
      {
        cell.transform.SetParent(this.transform, false);
        cell.RT().SetAnchors(Anchors.MiddleLeft);
      }
      else
      {
        Cells[i - 1].AddChild(cell);
        cell.RT().SetAnchors(Anchors.MiddleRight);
      }

      cell.ID = i;
      cell.ParentRow = this;
      Cells.Add(i, cell);
    }

    CheckWidth();
  }
}
