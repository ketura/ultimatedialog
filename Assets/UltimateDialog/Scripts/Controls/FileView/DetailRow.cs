﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using RektTransform;

public class DetailRow : MonoBehaviour 
{
  public int Count;
  public Dictionary<int, DetailCell> Cells;

  [HideInInspector]
  public DetailView ParentView;

  private LayoutElement _le;

  public DetailCell this[int i]
  {
    get { return Cells[i]; }
    private set { Cells[i] = value; }
  }

  protected void Awake () 
  {
    Init();
  }

  protected void Init()
  {
    Cells = new Dictionary<int, DetailCell>();
  }

  void Start()
  {
    if(_le == null)
      _le = GetComponent<LayoutElement>();
    _le.preferredWidth = TotalWidth();
  }
  
  void Update () 
  {
    
  }

  public void CheckWidth()
  {
    if (_le == null)
      _le = GetComponent<LayoutElement>();
    _le.preferredWidth = TotalWidth();
  }

  public virtual void CreateRow(DetailCell CellPrefab)
  {
    for(int i = 0; i < Count; ++i)
    {
      DetailCell cell = Instantiate<DetailCell>(CellPrefab);

      if (i == 0)
      {
        cell.transform.SetParent(this.transform, false);
        cell.RT().SetAnchors(Anchors.MiddleLeft);
      }
      else
      {
        Cells[i - 1].AddChild(cell);
        cell.RT().SetAnchors(Anchors.MiddleRight);
      }

      cell.ID = i;
      cell.ParentRow = this;
      Cells.Add(i, cell);
    }
  }

  public float GetWidth(int column)
  {
    return Cells[column].RT().GetWidth();
  }

  public float[] GetWidths()
  {
    float[] widths = new float[Cells.Count];
    for (int i = 0; i < Cells.Count; ++i)
      widths[i] = Cells[i].RT().GetWidth();

    return widths;
  }

  public float TotalWidth()
  {
    float[] widths = GetWidths();
    float total = 0;
    foreach (float w in widths)
      total += w;

    return total;
  }

  public void SetWidths(float[] widths)
  {
    for (int i = 0; i < Mathf.Min(widths.Length, Cells.Count); ++i)
    {
      SetWidth(i, widths[i]);
    }
  }

  public void SetWidth(int column, float width)
  {
    Cells[column].RT().SetWidth(width);

    CheckWidth();
  }

  public virtual void CellClicked(int column)
  {
    ParentView.ColumnHeaderClicked(column);
  }
}
 