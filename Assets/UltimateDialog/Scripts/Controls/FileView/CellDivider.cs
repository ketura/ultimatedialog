﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using RektTransform;
using System;

[RequireComponent(typeof(DoubleClickHandler))]
public class CellDivider : MonoBehaviour, IDragHandler, IDoubleClickHandler
{
  public HeaderCell Parent;
  private RectTransform RT;

  void Awake()
  {
    RT = this.RT();
  }

  private DoubleClickHandler _clicker;
  void Start () 
  {
    _clicker = GetComponent<DoubleClickHandler>();
    _clicker.Handler = this;
  }

  public void OnDrag(PointerEventData eventData)
  {
    RT.position = new Vector2(eventData.position.x, RT.position.y);
    Parent.OnDividerMove();
  }

  public void OnPointerClick()
  {

  }

  public void OnPointerDoubleClick()
  {
    Debug.Log("Double-click");
    Parent.OnDividerDoubleClick();
  }
}
