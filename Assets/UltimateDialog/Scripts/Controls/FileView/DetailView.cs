﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using RektTransform;
using System.IO;
using System;

public class DetailView : MonoBehaviour 
{
  public DetailRow RowPrefab;
  public DetailCell CellPrefab;
  public GameObject ArrowPrefab;

  public GameObject Children;
  public HeaderRow MainRow;
  public string[] ColumnNames;
  public FileData.DataType[] ColumnTypes;

  public int CurrentColumn;

  private Dictionary<int, FileData.DataType> Columns;
  private Dictionary<int, DetailRow> Rows;
  private Dictionary<int, DetailRow> FolderRows;

  private RectTransform SelectionArrow;
  

  void Awake()
  {
    Rows = new Dictionary<int, DetailRow>();
    FolderRows = new Dictionary<int, DetailRow>();

    if (ColumnNames.Length < 1)
      Debug.Log("No column names for DetailView!");
  }

  void Start () 
  {
    if (MainRow == null)
    {
      GameObject go = new GameObject();
      go.AddComponent<RectTransform>();
      MainRow = go.AddComponent<HeaderRow>();
      go.AddComponent<LayoutElement>().preferredHeight = 20;
      VerticalLayoutGroup group = go.AddComponent<VerticalLayoutGroup>();
      group.childForceExpandHeight = false;
      group.childForceExpandWidth = false;

      MainRow.transform.SetParent(Children.transform, false);
    }

    MainRow.Count = ColumnNames.Length;
    MainRow.ParentView = this;
    MainRow.CreateRow(CellPrefab);
    
    for(int i = 0; i < ColumnNames.Length; ++i)
      MainRow[i].text = ColumnNames[i];

    SelectionArrow = Instantiate(ArrowPrefab).RT();
    SelectionArrow.SetParent(this.transform, false);
    ColumnHeaderClicked(0);
  }
  
  void Update () 
  {
    
    //Debug.Log(TargetCell.ChildToParentSpace(TargetCell.GetTop()));
  }

  public void AlignRows()
  {
    float[] widths = MainRow.GetWidths();
    foreach (DetailRow row in Rows.Values)
      row.SetWidths(widths);
  }

  public DetailRow AddRow(params string[] info)
  {
    DetailRow row = Instantiate<DetailRow>(RowPrefab);
    row.transform.SetParent(Children.transform, false);
    row.Count = ColumnNames.Length;
    row.ParentView = this;
    row.CreateRow(CellPrefab);

    for (int i = 0; i < Mathf.Min(info.Length, ColumnNames.Length); ++i)
      row[i].text = info[i];

    row.SetWidths(MainRow.GetWidths());

    Rows.Add(Rows.Count, row);
    return row;
  }

  public void AddClick()
  {
    DateTime start = new DateTime(1995, 1, 1);
    System.Random gen = new System.Random();

    int range = (DateTime.Today - start).Days;
    start.AddDays(gen.Next(range));

    string filename = Path.GetRandomFileName();
    string ext = Path.GetExtension(filename).TrimStart('.') + " file";

    AddRow(filename, start.AddDays(gen.Next(range)).ToString(), ext, "20 TB", start.AddDays(gen.Next(range)).ToString());
  }

  public void FitWidth(int column)
  {
    float maxWidth = MainRow[column].PreferredWidth() ;

    foreach(DetailRow row in Rows.Values)
      maxWidth = Mathf.Max(maxWidth, row[column].PreferredWidth() );

    foreach (DetailRow row in Rows.Values)
      row.SetWidth(column, maxWidth);

    MainRow.SetWidth(column, maxWidth);
  }

  public void DividerAdjust(int column, float width)
  {
    foreach (DetailRow row in Rows.Values)
      row.SetWidth(column, width);
  }

  public void ColumnHeaderClicked(int column)
  {
    Debug.Log("Sorting " + column);

    foreach(DetailRow row in Rows.Values)
    {

    }


    if (column == CurrentColumn)
      SelectionArrow.transform.Rotate(0, 0, 180f);
    else
      SelectionArrow.transform.localRotation = Quaternion.Euler(0, 0, 180f);

    SelectionArrow.SetParent(MainRow[column].RT());
    SelectionArrow.SetAnchors(Anchors.TopCenter);
    SelectionArrow.MoveFrom(Anchors.TopCenter, 0, 0);

    CurrentColumn = column;
  }

  public Dictionary<int, string> GetColumn(int column)
  {
    Dictionary<int, string> data = new Dictionary<int, string>();

    foreach (DetailRow row in Rows.Values)
      data.Add(row[column].ID, row[column].text);

    return data;
  }
}
