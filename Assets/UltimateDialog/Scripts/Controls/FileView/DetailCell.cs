﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using RektTransform;

public class DetailCell : MonoBehaviour
{
  public Text Label;
  public GameObject Children;

  [HideInInspector]
  public DetailRow ParentRow;

  [HideInInspector]
  public int ID;

  public string text
  {
    get
    {
      if(Label != null)
        return Label.text;
      return "";
    }

    set
    {
      if(Label != null)
        Label.text = value;
    }
  }

  protected RectTransform RT;

  void Awake () 
  {
    RT = this.RT();
  }

  public void AddChild(DetailCell cell)
  {
    cell.transform.SetParent(Children.transform, false);
    RT.MoveLeft();
  }

  public virtual float PreferredWidth()
  {
    return Label.preferredWidth + 10;
  }
}
