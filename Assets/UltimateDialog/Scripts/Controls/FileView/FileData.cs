﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class FileData 
{
  public enum DataType { None, Path, Name, Modified, Created, Type, Size, Hidden }
  public string path { get; protected set; }
  public bool exists { get; protected set; }
  public bool isDirectory { get; protected set; }
  public string filename { get; protected set; }
  public Sprite icon { get; set; }
  public DateTime modified { get; protected set; }
  public DateTime created { get; protected set; }
  public string type { get; protected set; }
  public long size { get; protected set; }
  public bool hidden { get; protected set; }

  protected FileAttributes Attributes;
  protected FileSystemInfo info;

  public static bool FromPath(string path, ref FileData data)
  {
    data = new FileData();
    data.path = path;

    return data.TryReload();
  }

  protected FileData()
  {
    path = "";
    exists = false;
    isDirectory = false;
    filename = "";
    icon = null;
    modified = DateTime.Now;
    created = DateTime.Now;
    type = "File";
    size = 0;
    hidden = true;
  }

  public bool TryReload()
  {
    try
    {
      Reload();
    }
    catch (FileNotFoundException e)
    {
      Debug.LogError("File does not exist: " + path);
      exists = false;
      return false;
    }
    catch (DirectoryNotFoundException e)
    {
      Debug.LogError("Directory does not exist: " + path);
      isDirectory = true;
      exists = false;
      return false;
    }
    catch (Exception e)
    {
      Debug.LogError("Error loading " + path + ": " + e);
      throw;
    }

    return true;
  }

  public void Reload()
  {
    Attributes = File.GetAttributes(path);

    if (path == "")
    {
      Debug.Log("Blank path passed to " + this + " to reload.");
      //throw new InvalidOperationException("FileData needs initialized before calling Reload.  Call FileData.FromFile to properly initialize.");
      return;
    }

    exists = true;

    isDirectory = HasAttrib(FileAttributes.Directory);
    hidden = HasAttrib(FileAttributes.Hidden);

    if (isDirectory)
    {
      filename = Path.GetDirectoryName(path);
      type = "Directory";
      info = new DirectoryInfo(path);
    }
    else
    {
      filename = Path.GetFileName(path);
      type = Path.GetExtension(path) + " File";
      info = new FileInfo(path);
      size = (info as FileInfo).Length;
    }

    modified = Directory.GetLastWriteTime(path);
    created = Directory.GetCreationTime(path);
    
  }

  public  bool HasAttrib(FileAttributes flag)
  {
    return (Attributes & flag) == flag;
  }

}
