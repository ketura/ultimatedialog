﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Toggle))]
public class ToggleFix : MonoBehaviour 
{
  private Toggle _toggle;
  void Start () 
  {
    _toggle = GetComponent<Toggle>();
  }
  
  void Update () 
  {
    _toggle.graphic.gameObject.SetActive(_toggle.isOn);
  }
}
