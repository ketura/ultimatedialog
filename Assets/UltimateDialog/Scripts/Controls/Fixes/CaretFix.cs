﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;

public class CaretFix : MonoBehaviour, ISelectHandler
{
  private bool alreadyFixed;
  public Vector2 Adjust;

  public void OnSelect(BaseEventData eventData)
  {
    StartCoroutine(FixCaret());
    
  }

  IEnumerator FixCaret()
  {
    if (alreadyFixed) yield break;
    alreadyFixed = true;

    //This is the entire reason for the coroutine, since apparently the caret object is created AFTER
    // OnSelect is called.
    yield return null;

    string nm = gameObject.name + " Input Caret";
    RectTransform caretRT = (RectTransform)transform.Find(nm);
    //Debug.Log(caretRT);
    Vector2 newpos = caretRT.anchoredPosition;

    //Debug.Log("here's the ap .. " + newpos);

    newpos.y = newpos.y + Adjust.y;
    newpos.x = newpos.x + Adjust.x;

    caretRT.anchoredPosition = newpos;

    //Debug.Log("  here's a somewhat better ap .. " + newpos);
  }

  
  
}