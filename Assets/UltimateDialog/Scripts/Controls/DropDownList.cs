﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public interface IListEntryChangedHandler
{
  void OnListEntryChanged(DropDownList list, DropDownItem NewItem);
}

public class DropDownList : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
  public float FadeDelay = 1.0f;
  public string DefaultText = "Please select an option.";

  public GameObject ListPanel;
  public GameObject ItemPrefab;
  public Text CurrentItemText;
  public virtual string CurrentText
  {
    get
    {
      return CurrentItemText.text;
    }

    set
    {
      CurrentItemText.text = value;
    }
  }

  public DropDownItem CurrentItem { get; private set; }

  protected Dictionary<string, DropDownItem> Items;
  protected List<IListEntryChangedHandler> Handlers;

  protected DropDownItem NullItem;

	protected void Awake () 
  {
    Handlers = new List<IListEntryChangedHandler>();
    Items = new Dictionary<string, DropDownItem>();

    foreach (DropDownItem item in GetComponentsInChildren<DropDownItem>())
    {
      if (!Items.ContainsKey(item.text))
      {
        AddListItem(item);

        if (item.text == DefaultText)
          CurrentItem = item;
      }
      else
      {
        Debug.Log("Duplicate entry.  Destroying list item with label' " + item.text + "'.");
        Destroy(item.gameObject);
      }
    }

    NullItem = Instantiate(ItemPrefab).GetComponent<DropDownItem>();
    NullItem.enabled = false;
    NullItem.text = DefaultText;
    NullItem.name = gameObject.name + "_Null_Item";

    SetCurrentItem(NullItem);

    ListPanel.SetActive(false);
  }

  public void OnPointerEnter(PointerEventData eventData)
  {
    fading = false;
  }

  public void OnPointerExit(PointerEventData eventData)
  {
    Hide();
  }

  public void ToggleShow()
  {
    if (ListPanel.activeSelf)
      Hide(0);
    else
      ListPanel.SetActive(true);
  }

  public void Hide()
  {
    Hide(0);
  }

  public void Hide(float delay)
  {
    StartCoroutine(Fade(delay));
  }

  private bool fading = false;
  IEnumerator Fade(float delay)
  {
    if(!fading)
    {
      fading = true;
      if(delay > 0)
        yield return new WaitForSeconds(delay);

      if (fading)
      {
        foreach (DropDownItem item in Items.Values)
          item.SetHover(false);

        ListPanel.SetActive(false);
      }

      fading = false;
    }
  }

  public void ChildClicked(DropDownItem item)
  {
    SetCurrentItem(item);

    StartCoroutine(Fade(0));
  }

  public void AddListItem(string label)
  {
    if (Items.ContainsKey(label))
      return;

    DropDownItem newitem = Instantiate(ItemPrefab).GetComponent<DropDownItem>();
    newitem.parent = this;
    newitem.transform.SetParent(ListPanel.transform, false);
    newitem.text = label;

    AddListItem(newitem);
  }

  public void RemoveListItem(string label, bool purge=true)
  {
    RemoveListItem(label, purge);
  }

  public void AddListItem(DropDownItem item)
  {
    if (item != null)
    {
      Items[item.text] = item;
      item.parent = this;
    }
  }

  public void SetCurrentItem(DropDownItem item)
  {
    if (Items.ContainsKey(item.text))
    {
      CurrentItem = Items[item.text];
      CurrentText = item.text;

      AlertHandlers(item);
    }
    else
      ClearCurrentItem();
  }

  public void SetCurrentItem(string label)
  {
    SetCurrentItem(Items[label]);
  }

  public void ClearCurrentItem()
  {
    CurrentItem = NullItem;
    NullItem.text = DefaultText;
    CurrentText = DefaultText;

    AlertHandlers(NullItem);
  }

  protected void AlertHandlers(DropDownItem item)
  {
    foreach (IListEntryChangedHandler handler in Handlers)
      handler.OnListEntryChanged(this, item);
  }

  public void RemoveListItem(DropDownItem item, bool purge = true)
  {
    if (item != null)
    {
      Items.Remove(item.text);

      if (item == CurrentItem)
      {
        if (Items.Count > 0)
        {
          //This retardedness is due to .NET 2.0 not having any sort of GetElementAt for the generics.
          foreach (var value in Items.Values)
          {
            SetCurrentItem(value.text);
            break;
          }
        }
        else
          SetCurrentItem(NullItem);
      }

      if (purge)
        Destroy(item.gameObject);
    }
  }

  public void RegisterHandler(IListEntryChangedHandler handler)
  {
    if (handler != null && !Handlers.Contains(handler))
      Handlers.Add(handler);
  }

  public void RemoveHandler(IListEntryChangedHandler handler)
  {
    if (handler != null && !Handlers.Contains(handler))
      Handlers.Remove(handler);
  }

  public void OnPointerClick(PointerEventData eventData)
  {
    ToggleShow();
  }
}
