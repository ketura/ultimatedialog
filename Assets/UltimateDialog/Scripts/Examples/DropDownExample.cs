﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class DropDownExample : MonoBehaviour, IListEntryChangedHandler
{
  public DropDownList Combobox;
  public TextDropDown NameBox;

  public SpriteRenderer Subject;

  public Text NameText;

  public void OnListEntryChanged(DropDownList list, DropDownItem NewItem)
  {
    if (list == Combobox)
    {
      switch (NewItem.text)
      {
        case "Red":
          Subject.color = Color.red;
          break;
        case "Blue":
          Subject.color = Color.blue;
          break;
        case "Green":
          Subject.color = Color.green;
          break;
        default:
          Subject.color = Color.white;
          break;
      }
    }
    else if(list == NameBox)
    {
      NameText.text = "Hello! My name is\n" + NameBox.CurrentText;
    }
  }

  void Start()
  {
    Combobox.AddListItem("Red");
    Combobox.AddListItem("Blue");
    Combobox.AddListItem("Green");
    Combobox.RegisterHandler(this);
    NameBox.RegisterHandler(this);
  }

  void Update()
  {

  }
}
