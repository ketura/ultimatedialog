﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Original code from http://answers.unity3d.com/questions/357033/unity3d-and-c-coroutines-vs-threading.html
// original author Bunny83

public class ThreadedJob  
{
  private object _handle = new object();
  private System.Threading.Thread _thread = null;
  

  private bool _isDone;
  public bool IsDone
  {
    get
    {
      bool temp;
      lock(_handle)
      {
        temp = _isDone;
      }

      return temp;
    }

    set
    {
      lock(_handle)
      {
        _isDone = value;
      }
    }
  }

  private string _error;
  public string error
  {
    get
    {
      string temp;
      lock (_handle)
      {
        temp = _error;
      }

      return temp;
    }

    set
    {
      lock (_handle)
      {
        _error = value;
      }
    }
  }

  public virtual void Start () 
  {
    _thread = new System.Threading.Thread(Run);
    _thread.Start();
	}

  public virtual void Abort()
  {
    _thread.Abort();
  }

  protected virtual void ThreadFunction() { }

  protected virtual void OnFinished()
  {
    if (!string.IsNullOrEmpty(error))
      Debug.Log(error);
  }

  public virtual bool Update () 
  {
    if (_isDone)
    {
      OnFinished();
      return true;
    }

    return false;
	}

  public IEnumerator WaitFor()
  {
    while (!Update())
      yield return null;
  }

  private void Run()
  {
    ThreadFunction();
    IsDone = true;
  }
}

public class ThreadedQueue
{

}
