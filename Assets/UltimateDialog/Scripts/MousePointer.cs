﻿using UnityEngine;
using System.Collections;

public class MousePointer : Singleton<MousePointer>
{
  public Texture2D CurrentPointer;
  public CursorMode cursorMode = CursorMode.Auto;
  public Vector2 hotspot;

  public void ChangeCursor(Texture2D image, Vector2 spot)
  {
    CurrentPointer = image;
    hotspot = spot;
    Cursor.SetCursor(CurrentPointer, hotspot, cursorMode);
  }

  public void ClearCursor()
  {
    Cursor.SetCursor(null, Vector2.zero, cursorMode);
  }

  public void EnableCursor()
  {
    Cursor.visible = true;
  }

  public void DisableCursor()
  {
    Cursor.visible = false;
  }
}
