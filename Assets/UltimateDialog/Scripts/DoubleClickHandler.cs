﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public interface IDoubleClickHandler
{
  void OnPointerClick();
  void OnPointerDoubleClick();
}

public class DoubleClickHandler : MonoBehaviour, IPointerClickHandler
{
  public IDoubleClickHandler Handler;

  public float Threshold = 0.2f;

  private bool click = false;
  public void OnPointerClick(PointerEventData eventData)
  {
    if(!click)
    {
      click = true;
      StartCoroutine(ClickDelay());
      Handler.OnPointerClick();
    }
    else
    {
      Handler.OnPointerDoubleClick();
      click = false;
    }
  }

  IEnumerator ClickDelay()
  {
    yield return new WaitForSeconds(Threshold);
    if (click)
      click = false;
  }
}
