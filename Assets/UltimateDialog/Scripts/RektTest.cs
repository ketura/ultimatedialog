﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using RektTransform;
using System;

public class RektTest : MonoBehaviour, IDragHandler
{
  public enum RectFunction { None, Direction, MoveDir, MoveDirInside, MoveDirOutside, SetDir, SetDirFromDir, SetDirFromOpposite, SetDirWorld}

  public Dropdown dropdown;
  public Text Label;

  public InputField TopInput;
  public InputField BottomInput;
  public InputField LeftInput;
  public InputField RightInput;

  public Vector2 Size;
  public Vector2 LeftRight;

  public RectFunction CurrentFunction;

  private RectTransform RT;
  private Rect lastframe;
  void Awake () 
  {
    RT = transform as RectTransform;
    lastframe = RT.rect;
  }

  void Start()
  {
    OptionChanged();
    Debug.Log(RT.position);
    Debug.Log("0, 0: " + RT.ParentToChildSpace(0, 0));
    //RT.anchoredPosition = RT.ParentToChildSpace(0, 0);

    Debug.Log(RT.GetParent().rect);
    Debug.Log(RT.GetParent().rect.xMin);
    Debug.Log(RT.GetParent().rect.xMax);
    Debug.Log(RT.GetParent().rect.yMin);
    Debug.Log(RT.GetParent().rect.yMax);
    Debug.Log(RT.GetParent().GetWorldRect());
    Debug.Log(RT.GetParent().GetWorldRect().xMin);
    Debug.Log(RT.GetParent().GetWorldRect().xMax);
    Debug.Log(RT.GetParent().GetWorldRect().yMin);
    Debug.Log(RT.GetParent().GetWorldRect().yMax);

    Debug.Log(RT.position);

    //RT.position = RT
  }

  public void OptionChanged()
  {
    CurrentFunction =  (RectFunction)dropdown.value;
    Debug.Log(CurrentFunction);
  }

  private bool processing = false;
  void AdjustSizes()
  {
    if (!processing)
    {
      processing = true;
      RectTransform parent = transform.parent as RectTransform;
      //RT.SetPos(LeftRight);

      //RT.SetWidth(Size.x);
      //RT.SetHeight(Size.y);
      //RT.SetLeft(LeftRight.x);
      //RT.SetRightEdge(LeftRight.y);


      /*
      BotSlider.minValue = 0;
      BotSlider.maxValue = parent.GetHeight() / 2 - RT.GetHeight() / 2;
      BotSlider.SetHeight(BotSlider.maxValue);

      TopSlider.minValue = parent.GetHeight() / 2 + RT.GetHeight() / 2;
      TopSlider.maxValue = parent.GetHeight();
      TopSlider.SetHeight(BotSlider.maxValue);

      LeftSlider.minValue = 0;
      LeftSlider.maxValue = parent.GetWidth() / 2 - RT.GetWidth() / 2;
      LeftSlider.SetWidth(LeftSlider.maxValue);

      RightSlider.minValue = parent.GetWidth() / 2 + RT.GetWidth() / 2;
      RightSlider.maxValue = parent.GetWidth();
      RightSlider.SetWidth(LeftSlider.maxValue);
      */

      //yield return null;
      //TopSlider.normalizedValue = top;
      //BotSlider.normalizedValue = bot;
      //LeftSlider.normalizedValue = left;
      //RightSlider.normalizedValue = right;


      processing = false;
    }
  }
  
  void Update () 
  {
    AdjustSizes();
    if (lastframe != RT.rect)
    {
      lastframe = RT.rect;
      //RT.DebugOutput();
    }

    if (Input.GetMouseButton(1))
      Debug.Log(Input.mousePosition);

    Vector2 test = RT.anchoredPosition;
    Label.text = test.x + ", " + test.y;

    RT.MoveLeftInside();
    //RT.SetLeftFrom(Anchors.CenterRight, 0);
    //RT.MoveFrom(Anchors.TopLeft, 0, 0);
    //RT.SetRightFrom(Anchors.TrueCenter, 0);
  }

  public void OnDrag(PointerEventData eventData)
  {
    Vector2 offset = new Vector2(Screen.width / 2, Screen.height / 2);
    RT.Move(eventData.position - offset);
    //Debug.Log("\tMouse pos: " + (eventData.position - offset));
    //RT.DebugOutput();
  }

  public void MoveTop()
  {
    Debug.Log("\tAdjusting Top: " );
    RT.DebugOutput();
    //RT.MoveTopEdgeFromParent(LeftRight.x);
    RT.MoveTop(LeftRight.x);
    //RT.SetTopEdge(LeftRight.y);
    RT.DebugOutput();
  }

  public void MoveBot()
  {
    Debug.Log("\tAdjusting Bot: " );
    RT.DebugOutput();
    //RT.MoveBottomEdgeFromParent(LeftRight.y);
    RT.MoveBottom(LeftRight.y);
    //RT.SetBottomEdge(LeftRight.x);
    RT.DebugOutput();
  }

  public void MoveLeft()
  {
    Debug.Log("\tAdjusting Left: " + CurrentFunction );
    //RT.DebugOutput();
    float left = float.Parse(LeftInput.text);
    switch(CurrentFunction)
    {
      case RectFunction.Direction:
        RT.Left(left);
        break;

      case RectFunction.MoveDir:
        RT.MoveLeft(left);
        break;

      case RectFunction.MoveDirInside:
        RT.MoveLeftInside(left);
        break;

      case RectFunction.MoveDirOutside:
        RT.MoveLeftOutside(left);
        break;

      case RectFunction.SetDir:
        //RT.SetLeft(left);
        break;

      case RectFunction.SetDirFromDir:
        RT.SetLeft(left);
        break;

      case RectFunction.SetDirFromOpposite:
        RT.SetLeftFrom(Anchors.MiddleRight, left);
        break;

      case RectFunction.SetDirWorld:
        //RT.SetLeftWorld(left);
        break;
    }
    //RT.MoveLeftEdgeFromParent(LeftRight.x);
    //RT.MoveLeft(LeftRight.x);
    //RT.SetLeftEdge(LeftRight.x);
    //RT.DebugOutput();
  }

  public void MoveRight()
  {
    Debug.Log("\tAdjusting Right: " );
    //RT.DebugOutput();
    //RT.MoveRightEdgeFromParent(LeftRight.y);
    float right = float.Parse(RightInput.text);
    switch (CurrentFunction)
    {
      case RectFunction.Direction:
        RT.Right(right);
        break;

      case RectFunction.MoveDir:
        RT.MoveLeft(right);
        break;

      case RectFunction.MoveDirInside:
        RT.MoveLeftInside(right);
        break;

      case RectFunction.MoveDirOutside:
        RT.MoveLeftOutside(right);
        break;

      case RectFunction.SetDir:
        RT.SetRight(right);
        break;

      case RectFunction.SetDirFromDir:
        RT.SetRight(right);
        break;

      case RectFunction.SetDirFromOpposite:
        RT.SetRightFrom(Anchors.MiddleLeft, right);
        break;

      case RectFunction.SetDirWorld:
        //RT.SetLeftWorld(right);
        break;
    }
    //RT.MoveRight(LeftRight.y);
    //RT.SetRightEdge(LeftRight.y);
    //RT.DebugOutput();
  }
}
