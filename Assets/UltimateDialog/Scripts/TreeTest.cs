﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TreeTest : MonoBehaviour 
{
  public Button button;
  public InputField input;

  public TreeView Tree;

  void Start () 
  {
    button.onClick.AddListener(Clicked);
  }
  
  void Update () 
  {
  
  }

  public void Clicked()
  {
    Tree.AddChild(input.text);
  }
}
